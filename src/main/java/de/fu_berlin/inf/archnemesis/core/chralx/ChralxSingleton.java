package de.fu_berlin.inf.archnemesis.core.chralx;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import de.fu_berlin.inf.archnemesis.archnemesis.Architecture;
import de.fu_berlin.inf.archnemesis.archnemesis.BiDirectional;
import de.fu_berlin.inf.archnemesis.archnemesis.Component;
import de.fu_berlin.inf.archnemesis.archnemesis.Connector;
import de.fu_berlin.inf.archnemesis.archnemesis.Constraint;
import de.fu_berlin.inf.archnemesis.archnemesis.Element;
import de.fu_berlin.inf.archnemesis.archnemesis.Tangle;
import de.fu_berlin.inf.archnemesis.archnemesis.UniDirectional;

public class ChralxSingleton {
    public static String defaultChralxLocation = "architecture.chralx";
    private static ChralxSingleton instance;
    private final Properties chralxProperties = new Properties();

    private Architecture arc;
    private Map<Component, Set<Component>> componentToAccess;
    private final List<Constraint> constraints = new ArrayList<Constraint>();

    private ChralxSingleton(String chralxLocation) {
        chralxProperties.setProperty("chralxLocation", chralxLocation);
        String chralxAbsolutePath = getChralxAbsolutePath();

        if (doesntExist(chralxAbsolutePath))
            return;

        arc = parseChralx(chralxAbsolutePath);
        parseArc();
    }

    public static ChralxSingleton getInstance() {
        if(instance == null) {
            instance = new ChralxSingleton(defaultChralxLocation);
        }
        return instance;
    }

    public static ChralxSingleton getInstance(String location) {
        if(instance == null) {
            instance = new ChralxSingleton(location);
        }
        return instance;
    }

    public Architecture getArc() {
        return arc;
    }

    public Map<Component, Set<Component>> getComponentToAccess() {
        return componentToAccess;
    }

    private String getChralxAbsolutePath() {
        String chralxLocation = chralxProperties.getProperty("chralxLocation");
        if (chralxLocation.equals(defaultChralxLocation)) {
            return new File(defaultChralxLocation).getAbsolutePath();
        }
        return new File(chralxLocation).getAbsolutePath();
    }

    private void parseArc() {
        Map<Component, Set<Component>> accessMap = new HashMap<Component, Set<Component>>();

        List<Tangle> tangles = new ArrayList<Tangle>();
        List<Component> components = new ArrayList<Component>();
        List<Connector> connectors = new ArrayList<Connector>();

        for (Element e : arc.getElements()) {
            if (e.getTangle() != null) {

                Tangle tangle = e.getTangle();

                if (tangle != null) {
                    if (tangle.getComponent() != null)
                        components.add(tangle.getComponent());
                    else if (tangle.getConnector() != null)
                        connectors.add(tangle.getConnector());
                    else if (tangle.getConstraint() != null) {
                        constraints.add(tangle.getConstraint());
                    }
                }
            }
        }

        // init
        for (Component c : components) {
            Set<Component> allowedAccess = new HashSet<Component>();
            allowedAccess.add(c); // Component is allowed to access itself
            accessMap.put(c, allowedAccess);
        }

        // create mapping of component to allowed access
        for (Connector c : connectors) {
            if (c instanceof UniDirectional) {
                Component from = ((UniDirectional) c).getFrom();
                Component to = ((UniDirectional) c).getTo();
                accessMap.get(from).add(to);
            } else if (c instanceof BiDirectional) {
                Component comp1 = ((BiDirectional) c).getComp1();
                Component comp2 = ((BiDirectional) c).getComp2();
                accessMap.get(comp1).add(comp2);
                accessMap.get(comp2).add(comp1);
            }
        }
        componentToAccess = accessMap;
    }

    public static boolean noArchitectureDefinitionFound() {
        return !(new File(defaultChralxLocation)).exists();
    }

    public boolean doesntExist(String chralxAbsolutePath) {
        return !(new File(chralxAbsolutePath)).exists();
    }

    Architecture parseChralx(String absoluteChralxPath) {
        XtextParser p = new XtextParser();
        return (Architecture) p.parse(absoluteChralxPath);
    }

    public List<Constraint> getConstraints() {
        return constraints;
    }
}